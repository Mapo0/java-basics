package com.company;
// array-1
//Given an array of ints, return true if 6 appears as either the first or last element in the array.
// The array will be length 1 or more.

import java.util.Arrays;

public class Main {
    public boolean firstLast6(int[] nums) {
        return nums[0] == 6 || nums[nums.length - 1] == 6;
    }

    public static void main(String[] args) {
	    Main main = new Main();
	    System.out.print(main.firstLast6(new int[]{1, 2, 3, 4, 5, 6}));
    }
}
