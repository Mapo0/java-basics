package com.company;
//String 1
//Given a string name, e.g. "Bob", return a greeting of the form "Hello Bob!"

public class Main {
    public String helloName(String name) {
        return "Hello " + name + "!";
    }

    public static void main(String[] args) {
	Main main= new Main();
	System.out.print(main.helloName("Vladimir"));
    }
}
