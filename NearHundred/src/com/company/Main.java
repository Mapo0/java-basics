package com.company;
//Given an int n, return true if it is within 10 of 100 or 200.
// Note: Math.abs(num) computes the absolute value of a number.

public class Main {
    public boolean nearHundred(int n) {
        if ((Math.abs(100 - n) <= 10) || (Math.abs(200 - n) <= 10))
            return true;
        else
            return false;
    }

    public static void main(String[] args) {
        Main main = new Main();
        System.out.print(main.nearHundred(89));
    }
}
