package com.company;
// warmup-1
//The parameter weekday is true if it is a weekday, and the parameter vacation is true if we are on vacation.
// We sleep in if it is not a weekday or we're on vacation. Return true if we sleep in.

public class Main {
    public boolean sleepIn(boolean weekday, boolean vacation) {
        if (!weekday || vacation )
        return true;
        else
            return false;
    }


    public static void main(String[] args) {
	    Main main=new Main();
	    System.out.print(main.sleepIn(true,false));
    }
}
