package com.company;
//Warmup-2
//Given a string and a non-negative int n,
// return a larger string that is n copies of the original string.

public class Main {
    public String stringTimes(String str, int n) {
        String s= "";
        for (int i=0; i<n; i++)
            s=s+str ;
        return s;
    }

    public static void main(String[] args) {
	Main main=new Main();
	System.out.print(main.stringTimes("HI",7));
    }
}
