package com.company;
//string 1
//Given a string of even length, return the first half. So the string "WooHoo" yields "Woo".

public class Main {
    public String firstHalf(String str) {
        return str.substring(0,str.length()/2);

    }


    public static void main(String[] args) {
	Main main=new Main();
	System.out.print(main.firstHalf("qwerty"));
    }
}
